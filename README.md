## How to install and run API part

1. Clone git repo
2. cd api
3. npm install
4. cd api and copy application.default.json -> application.json
5. use dq.sql to create and populate database structure
6. npm run (it will lunch Node app on port 3000)

## How to install and run Front part

1. Clone git repo
2. cd front
3. npm install
4. npm run-script dev (it will lunch dev server on port 3001)