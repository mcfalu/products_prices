'use strict';

const express = require('express');
const validator = require('express-validator');
const bodyParser = require('body-parser');
const log4js = require('log4js');
const db = require('./db');
const response = require('./modules/response');

const conf = require('./modules/config');

log4js.configure({
  appenders: { cheese: { type: 'console' } },
  categories: { default: { appenders: ['cheese'], level: 'info' } }
});

const app = express();

app.enable('trust proxy');
app.use(bodyParser.json({ limit: '1mb' }));
app.use(bodyParser.urlencoded({ extended: true, limit: '1mb' }));
app.use(validator());

app.use((req, res, next) => {

  res.header('X-powered-by', '');

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');

  next();

});


function normalizePort (val) {

  const port = parseInt(val, 10);
  if (isNaN(port)) return val;

  if (port >= 0) return port;

  return false;

}

const port = normalizePort(conf.get('port') || '3000');
app.set('port', port);

app.use(response());

module.exports = app;