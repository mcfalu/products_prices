const products = require('../modules/products');
const stores = require('../modules/stores');

/**
 * Get list items
 *
 * @param req
 * @param res
 */
module.exports.items = (req, res) => {

  products.getItems().then(items => {

    if (!items) return res.status(200).send([]);

    return res.status(200).send(items.map(i => {
      return {
        id: i.id,
        name: i.name,
        type: i.type
      }
    }));

  }).catch(err => res.serverError(err));

};

/**
 * Get item
 *
 * @param req
 * @param res
 */
module.exports.getItem = (req, res) => {

  const id = req.params.id;

  products.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    return res.status(200).send(item);

  }).catch(err => res.serverError(err));

};

/**
 * Get item prices
 *
 * @param req
 * @param res
 */
module.exports.getItemPrices = (req, res) => {

  const id = req.params.id;

  products.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    products.getItemPrices(id).then(data => {

      return res.status(200).send(data);

    }).catch(err => res.serverError(err));

  }).catch(err => res.serverError(err));

};

/**
 * Update item prices
 *
 * @param req
 * @param res
 */
module.exports.updateItemPrices = (req, res) => {

  const id = req.params.id;

  products.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    const storeId = req.body.store_id ? req.body.store_id : null;

    stores.getItem(storeId).then(store => {

      if (!store) return res.invalidInputValues({
        field: 'store_id',
        message: 'Store is not found'
      });

      const price = req.body.price ? req.body.price : 0;

      products.updateItemPrice(id, storeId, price)
        .then(result => {

          if (result) return res.status(200).send({});
          else return res.status(400).send({});

        }).catch(err => res.dbError(err));

    }).catch(err => res.serverError(err));

  }).catch(err => res.serverError(err));

};


/**
 * Create new item
 *
 * @param req
 * @param res
 */
module.exports.newItem = (req, res) => {

  products.newItem({
    name: req.body.name ? req.body.name : '',
    type: req.body.type ? req.body.type : ''
  }).then(item => res.status(201).send(item))
    .catch(err => res.dbError(err));


};

/**
 * Update item info
 *
 * @param req
 * @param res
 */
module.exports.editItem = (req, res) => {

  const id = req.params.id;

  products.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    if (req.body.name) item.name = req.body.name;
    if (req.body.type) item.type = req.body.type;

    products.editItem(id, item).then(r => {

      return res.status(200).send(item);

    }).catch(err => res.dbError(err));

  }).catch(err => res.serverError(err));

};

/**
 * Delete an item
 *
 * @param req
 * @param res
 */
module.exports.deleteItem = (req, res) => {

  const id = req.params.id;

  products.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    products.deleteItem(id, item).then(r => {

      if (r) return res.status(204).send({});
      else return res.status(400).send({});

    }).catch(err => res.serverError(err));

  }).catch(err => res.serverError(err));

};