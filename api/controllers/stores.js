const stores = require('../modules/stores');

/**
 * Get items
 *
 * @param req
 * @param res
 */
module.exports.items = (req, res) => {

  stores.getItems().then(items => {

    if (!items) return res.status(200).send([]);

    return res.status(200).send(items.map(i => {
      return {
        id: i.id,
        name: i.name,
        city: i.city
      }
    }));

  }).catch(err => res.serverError(err));

};

/**
 * Get item
 *
 * @param req
 * @param res
 */
module.exports.getItem = (req, res) => {

  const id = req.params.id;

  stores.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    return res.status(200).send(item);

  }).catch(err => res.serverError(err));

};

/**
 * Create new item
 *
 * @param req
 * @param res
 */
module.exports.newItem = (req, res) => {

  stores.newItem({
    name: req.body.name ? req.body.name : '',
    city: req.body.city ? req.body.city : ''
  }).then(item => res.status(201).send(item))
    .catch(err => res.dbError(err));

};

/**
 * Update item info
 *
 * @param req
 * @param res
 */
module.exports.editItem = (req, res) => {

  const id = req.params.id;

  stores.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    if (req.body.name) item.name = req.body.name;
    if (req.body.city) item.type = req.body.city;

    stores.editItem(id, item).then(r => {

      return res.status(200).send(item);

    }).catch(err => res.dbError(err));

  }).catch(err => res.serverError(err));

};

/**
 * Delete item
 *
 * @param req
 * @param res
 */
module.exports.deleteItem = (req, res) => {

  const id = req.params.id;

  stores.getItem(id).then(item => {

    if (!item) return res.notFound('product');

    stores.deleteItem(id, item).then(r => {

      if (r) return res.status(204).send({});
      else return res.status(400).send({});

    }).catch(err => res.serverError(err));

  }).catch(err => res.serverError(err));

};