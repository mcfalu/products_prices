'use strict';

const logger = require('log4js').getLogger('app');
const mysql = require('mysql');
const conf = require('./modules/config');

const connection = mysql.createConnection({
  host: conf.get('database:host'),
  user: conf.get('database:user'),
  password: conf.get('database:pass'),
  database: conf.get('database:name'),
  multipleStatements: true
});

connection.connect(err => {

  if (err) throw err;

  logger.info('Connected to database: ' + conf.get('database:name'));

});

module.exports.connection = connection;