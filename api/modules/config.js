'use strict';

const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const conf = require('nconf');

const NODE_ENV = process.env.NODE_ENV || 'dev';
const configDir = path.join(__dirname, '..', 'config');

const configs = _.merge(
  require(configDir + '/application.json')
);

conf.env().argv();
conf.defaults(configs);
conf.use('memory');

conf.getEnv = () => NODE_ENV;

module.exports = conf;