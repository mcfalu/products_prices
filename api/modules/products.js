const db = require('../db');

module.exports = {
  getItems: () => {
    return new Promise((resolve, reject) => {

      db.connection.query('SELECT * FROM products', (err, rows) => {

        if (err) return reject(err);

        return resolve(rows);

      });

    });
  },
  getItem: id => {

    return new Promise((resolve, reject) => {

      db.connection.query('SELECT * FROM products WHERE id = ?', id, (err, row) => {

        if (err) return reject(err);

        if (!row[0]) return resolve(null);

        return resolve({
          id: row[0].id,
          name: row[0].name,
          type: row[0].type
        });

      });

    });

  },
  newItem: data => {

    return new Promise((resolve, reject) => {

      const item = { name: data.name, type: data.type };

      db.connection.query("CALL products_new(?, ?, @product_id); SELECT @product_id as product_id",
        [item.name, item.type],
        (err, res) => {

          if(err) {
            return reject(err);
          }

          item.id = res[1][0].product_id;

          return resolve(item);

        });

    });

  },

  editItem: (id, data) => {

    return new Promise((resolve, reject) => {

      db.connection.query('CALL products_edit_item(?, ?, ?)',
        [data.name, data.type, id] ,
        (err, res) => {

          if(err) return reject(err);

          if (res.changedRows) return resolve(true);

          return resolve(false);

        });


    });


  },
  deleteItem: id => {

    return new Promise((resolve, reject) => {

      db.connection.query('DELETE FROM products WHERE id = ?',
        id ,
        (err, res) => {

          if(err) return reject(err);

          if (res.affectedRows) return resolve(true);

          return resolve(false);

        });


    });

  },
  getItemPrices: id => {

    return new Promise((resolve, reject) => {

      db.connection.query('' +
        'SELECT s.id, s.name store_name, s.city store_city, pp.*' +
        ' FROM stores s' +
        ' LEFT JOIN products_prices pp ON (s.id = pp.store_id AND pp.product_id = ?)',
        id, (err, items) => {

        if (err) return reject(err);

        return resolve(items.map(i => {
          return {
            store_id: i.id,
            store_name: i.store_name,
            store_city: i.store_city,
            price: i.price !== null ? Number(i.price).toFixed(2) : ''
          }
        }));

      });

    });

  },
  updateItemPrice: (id, storeId, price) => {

    return new Promise((resolve, reject) => {

      db.connection.query('CALL product_price(?, ?, ?)',
        [id, storeId, price] ,
        (err, res) => {

          if(err) return reject(err);

          if (res.affectedRows) return resolve(true);

          return resolve(false);

        });


    });


  }

};