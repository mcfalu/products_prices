'use strict';

const logger = require('log4js').getLogger('app');

module.exports = () => (req, res, next) => {

  /**
   * Invalid input values
   *
   * @param errors
   * @returns {{error: {field: string, message: *}}}
   */
  res.invalidInputValues = function (errors) {

    const result = {
      errors: []
    };

    if (errors.field && errors.message) {

      result.errors.push({
        code: 'invalid_input_value',
        message: errors.message,
        field: errors.field
      });

    } else if (errors.length > 0) {

      const invalidFields = [];

      for (const i in errors) {

        if (errors[i] && invalidFields.indexOf(errors[i].param) === -1) {

          invalidFields.push(errors[i].param);

          result.errors.push({
            code: 'invalid_input_value',
            message: errors[i].msg,
            field: errors[i].param
          });

        }

      }

    }

    return this.status(400).send(result);

  };


  /**
   * Not found resource
   *
   * @param resource
   * @returns {*}
   */
  res.notFound = function (resource) {

    return this.status(404).send({
      errors: [
        {
          code: 'resource_not_found',
          message:  'The specified resource does not exist.',
          field: resource
        }
      ]
    });

  };


  /**
   * Internal server error
   *
   * @param error
   * @returns {*}
   */
  res.serverError = function (error) {

    logger.error(error.toString());

    return this.status(500).send({
      errors: [
        {
          code: 'internal_server_error',
          message: 'The server encountered an internal error. Please retry the request.',
          field: ''
        }
      ]
    });

  };

  res.dbError = function (error) {

    switch (error.code){

      case 'ER_SIGNAL_EXCEPTION' :

        return res.status(400).send({
          code: 'invalid_input_value',
          message: error.sqlMessage,
        });

        break;

      default:
        this.serverError(error);
    }

  };


  next();

};
