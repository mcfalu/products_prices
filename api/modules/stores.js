const db = require('../db');

module.exports = {
  getItems: () => {
    return new Promise((resolve, reject) => {

      db.connection.query('CALL stores_getitems()', (err, rows) => {

        if (err) return reject(err);

        return resolve(rows[0]);

      });

    });
  },
  getItem: id => {

    return new Promise((resolve, reject) => {

      db.connection.query('CALL stores_getitem(?)', id, (err, row) => {

        if (err) return reject(err);

        if (!row[0][0]) return resolve(null);

        return resolve({
          id: row[0][0].id,
          name: row[0][0].name,
          city: row[0][0].city
        });

      });

    });

  },
  newItem: data => {

    return new Promise((resolve, reject) => {

      const item = { name: data.name, city: data.city };

      db.connection.query("CALL stores_new(?, ?, @store_id); SELECT @store_id as store_id",
        [item.name, item.city],
        (err, res) => {

          if(err) {
            return reject(err);
          }

          item.id = res[1][0].store_id;

          return resolve(item);

        });

    });

  },

  editItem: (id, data) => {

    return new Promise((resolve, reject) => {

      db.connection.query('CALL stores_edit_item(?, ?, ?)',
        [data.name, data.city, id] ,
        (err, res) => {

          if(err) return reject(err);

          if (res.changedRows) return resolve(true);

          return resolve(false);

      });


    });

  },
  deleteItem: id => {

    return new Promise((resolve, reject) => {

      db.connection.query('DELETE FROM products WHERE id = ?',
        id ,
        (err, res) => {

          if(err) return reject(err);

          if (res.affectedRows) return resolve(true);

          return resolve(false);

        });


    });

  }

};