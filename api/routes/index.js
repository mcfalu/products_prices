'use strict';

const express = require('express');

const products = require('../controllers/products');
const stores = require('../controllers/stores');

module.exports.init = app => {

  app.get('/', (req, res) => {

    res.status(200).send({})

  });

  app.get('/products', products.items);

  app.post('/products', products.newItem);

  app.get('/products/:id', products.getItem);

  app.put('/products/:id', products.editItem);

  app.delete('/products/:id', products.deleteItem);

  app.get('/products/:id/prices', products.getItemPrices);

  app.post('/products/:id/prices', products.updateItemPrices);

  app.get('/stores', stores.items);

  app.post('/stores', stores.newItem);

  app.get('/stores/:id', stores.getItem);

  app.put('/stores/:id', stores.editItem);

  app.delete('/stores/:id', stores.deleteItem);

};



