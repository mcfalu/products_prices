-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2017 at 05:05 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `products_prices`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `products_edit_item` (IN `product_name` VARCHAR(255), IN `product_type` VARCHAR(255), IN `product_id` INT)  NO SQL
BEGIN

IF(product_name = '') THEN
 SIGNAL SQLSTATE '40011'
 SET MESSAGE_TEXT = 'Name is blank';
 END IF;

IF(product_type = '') THEN
 SIGNAL SQLSTATE '40012'
 SET MESSAGE_TEXT = 'Type is blank';
 END IF;

UPDATE products SET name = product_name, type = product_type WHERE id = product_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `products_new` (IN `product_name` VARCHAR(255), IN `product_type` VARCHAR(255), OUT `product_id` INT)  NO SQL
BEGIN

IF(product_name = '') THEN
 SIGNAL SQLSTATE '40001'
 SET MESSAGE_TEXT = 'Name is blank';
 END IF;

IF(product_type = '') THEN
 SIGNAL SQLSTATE '40002'
 SET MESSAGE_TEXT = 'Type is blank';
 END IF;

insert into products( name, type) values ( product_name, product_type );
set product_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `product_price` (IN `pid` INT(11), IN `sid` INT(11), IN `p_price` DECIMAL(10,2))  NO SQL
BEGIN

IF(p_price = 0) THEN
 SIGNAL SQLSTATE '40021'
 SET MESSAGE_TEXT = 'Price cannot be 0';
 END IF;

SET @price_exist = 0;

SELECT count(*) INTO @price_exist from products_prices WHERE product_id = pid AND store_id = sid;

IF (@price_exist > 0) THEN
  UPDATE products_prices SET price = p_price WHERE product_id = pid AND store_id = sid;
ELSE
  INSERT INTO products_prices (product_id, store_id, price ) VALUES(pid, sid, p_price);
END IF;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stores_edit_item` (IN `store_name` VARCHAR(255), IN `store_city` VARCHAR(255), IN `store_id` INT)  NO SQL
BEGIN

IF(store_name = '') THEN
 SIGNAL SQLSTATE '40001'
 SET MESSAGE_TEXT = 'Name is blank';
 END IF;

IF(store_city = '') THEN
 SIGNAL SQLSTATE '40002'
 SET MESSAGE_TEXT = 'City is blank';
 END IF;

UPDATE stores SET name = store_name, city = store_city WHERE id = store_id;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stores_getitem` (IN `store_id` INT)  NO SQL
SELECT id, name, city FROM stores where id = store_id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stores_getitems` ()  NO SQL
SELECT id, name, city FROM stores$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `stores_new` (IN `store_name` VARCHAR(255), IN `store_city` VARCHAR(255), OUT `store_id` INT)  NO SQL
BEGIN

IF(store_name = '') THEN
 SIGNAL SQLSTATE '40001'
 SET MESSAGE_TEXT = 'Name is blank';
 END IF;

IF(store_city = '') THEN
 SIGNAL SQLSTATE '40002'
 SET MESSAGE_TEXT = 'City is blank';
 END IF;

insert into stores( name, city) values ( store_name, store_city );
set store_id = LAST_INSERT_ID();
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` enum('BOOK','TOY','CLOTHES') NOT NULL DEFAULT 'BOOK'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `type`) VALUES
(19, 'Romeo and Juliet. Shakespeare', 'BOOK'),
(20, 'The Adventures of Tom Sawyer', 'BOOK'),
(21, 'Don Quixote by Miguel de Cervantes', 'BOOK'),
(22, 'War and Peace by Leo Tolstoy', 'BOOK'),
(23, 'The Odyssey by Homer', 'BOOK'),
(24, 'Pokémon Moon - Nintendo 3DS', 'TOY'),
(25, 'LEGO BATMAN MOVIE Mr. Freeze Ice Attack 70901 Building Kit', 'TOY'),
(26, 'Sony PlayStation 4', 'TOY'),
(27, 'xbox 360', 'TOY'),
(28, 'Zara T-Shirt Size L', 'CLOTHES'),
(29, 'Levis 514 Mens Jeans Slim Fit Straight Leg', 'CLOTHES');

-- --------------------------------------------------------

--
-- Table structure for table `products_prices`
--

CREATE TABLE `products_prices` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `city`) VALUES
(1, 'Best Buy', 'Kyiv'),
(2, 'Rozetka', 'Kyiv'),
(3, 'Amazon', 'New York'),
(4, 'Zara', 'Munich'),
(5, 'ToysLand', 'Paris');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_prices`
--
ALTER TABLE `products_prices`
  ADD PRIMARY KEY (`product_id`,`store_id`),
  ADD KEY `store_id` (`store_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `products_prices`
--
ALTER TABLE `products_prices`
  ADD CONSTRAINT `products_prices_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_prices_ibfk_2` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;