import axios from "axios";
import {API_URL} from "../config";
import {browserHistory} from 'react-router';

export function fetchProducts() {

  return function (dispatch) {

    axios.get(API_URL + "products")
      .then((response) => {

        let products = response.data ? response.data : [];

        dispatch({type: "FETCH_PRODUCTS", payload: products});

      })
      .catch((err) => {

        dispatch([{type: "FETCH_PRODUCTS_REJECTED", payload: err}])

      });
  }
}

export function fetchStores() {

  return function (dispatch) {

    axios.get(API_URL + "stores")
      .then((response) => {

        let products = [];
        if (response.data) {

          products = response.data;
        }

        dispatch({type: "FETCH_STORES", payload: products});

      })
      .catch((err) => {
        dispatch([{type: "FETCH_STORES_REJECTED", payload: err}])
      })
  }
}

export function addProduct(data) {

  return function (dispatch) {

    axios.post(API_URL + "products", data)
      .then((response) => {

        let data = [];
        if (response.data) {

          data = response.data;
        }

        browserHistory.push('/');

        dispatch({type: "PRODUCT_ADDED", payload: data});

      })
      .catch((err) => {

        if (err.response.data.message) {
          alert(err.response.data.message);
        }

        dispatch([{type: "PRODUCT_ADDED_FAILED", payload: err}])


      })
  }
}

export function editProduct (id, data) {

  return function (dispatch) {

    axios.put(API_URL + "products/" + id , data)
      .then((response) => {

        let data = [];
        if (response.data) {

          data = response.data;
        }

        browserHistory.push('/');

        dispatch({type: "PRODUCT_EDITED", payload: data});

      })
      .catch((err) => {

        if (err.response.data.message) {
          alert(err.response.data.message);
        }

        dispatch([{type: "PRODUCT_ADDED_FAILED", payload: err}])


      })
  }

}

export function removeProduct(id) {

  return function (dispatch) {

    axios.delete(API_URL + "products/" + id)
      .then((response) => {

        dispatch([{type: "PRODUCT_DELETED_SUCCESS", payload: id},
          (dispatch) => {
            dispatch(fetchProducts());
          }]);

      })
      .catch((err) => {
        dispatch([{type: "PRODUCT_DELETED_FAILED", payload: err}])
      })
  }

}

export function getProduct(id) {

  return function (dispatch) {

    axios.get(API_URL + "products/" + id)
      .then((response) => {

        dispatch([{type: "PRODUCT_FETCH_SUCCESS", payload: response.data}]);

      })
      .catch((err) => {
        dispatch([{type: "PRODUCT_FETCH_FAILED", payload: err}])
      })
  }

}

export function getProductPrices (id) {

  return function (dispatch) {

    axios.get(API_URL + "products/" + id + "/prices")
      .then((response) => {

        dispatch([{type: "FETCH_PRODUCT_PRICES", payload: response.data}]);

      })
      .catch((err) => {
        dispatch([{type: "FETCH_PRODUCT_PRICES_FAILED", payload: err}])
      })
  }

}

export function setProductPrice (productId, storeId, price) {

  return function (dispatch) {

    axios.post(API_URL + "products/" + productId + "/prices", {
      store_id: storeId,
      price: price
    })
      .then((response) => {

        dispatch([{type: "PRODUCT_PRICE_SAVED", payload: response.data}]);

      })
      .catch((err) => {

        if (err.response.data.message) {
          alert(err.response.data.message);
        }

        dispatch([{type: "PRODUCT_PRICE_FAILED", payload: err}])
      })
  }

}