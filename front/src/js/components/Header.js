import React from "react";
import {Link} from "react-router";

class Header extends React.Component {

  render() {

    return (
      <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="navbar-header">
            <a class="navbar-brand" href="#"></a>
          </div>
          <div id="navbar" class="navbar-collapse">

            <div>
              <ul className="nav navbar-nav navbar-left">
                <li>
                  <Link to="/products">Products</Link>
                </li>
                <li>
                  <Link to="/stores">Stores</Link>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </nav>
    );
  }

}

export default Header;