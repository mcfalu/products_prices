import React from "react";

import { addProduct, editProduct, getProduct } from "../../actions/productsActions"
import {Link} from 'react-router'
import {productTypes} from "../../helpers";

import { connect } from "react-redux"

class Product extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {

    if (this.props.params.id) {

      this.props.dispatch(getProduct(this.props.params.id));

    }

  }

  onSubmit = (e) => {

    e.preventDefault();

    let data = {
      name: this.refs.name.value,
      type: this.refs.type.value
    };

    if (this.props.params.id) {
      this.props.dispatch(editProduct(this.props.params.id, data));
    } else {
      this.props.dispatch(addProduct(data));
    }

  };

  render() {

    let {product} = this.props;

    if (this.props.params.id && !product.id) {
      return (
        <div>Loading...</div>
      );
    }

    return (
      <div className="container">
        <div className="col-lg-12">

          <h3>Product</h3>

          <form method="post">

            <div className="form-group">
              <label>Name</label>

              <input type="text"
                     ref="name"
                     className="form-control"
                     placeholder="Name"
                     defaultValue={product.name} />
            </div>

            <div className="form-group">
              <label>Type</label>
              <select className="form-control" ref="type">
                <option value="" />
                {Object.keys(productTypes).map((item, i) => {
                  return <option value={item} key={i} selected={product.type == item}>{productTypes[item]}</option>
                })}
              </select>

            </div>

            <button type="button" className="btn btn-default" onClick={this.onSubmit}>Save</button>

            <Link className="btn btn-link" to="products">
              Cancel
            </Link>
          </form>

        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    product: state.products.product
  };
})(Product)