import React from "react";

import { getProduct, getProductPrices, setProductPrice } from "../../actions/productsActions"

import { connect } from "react-redux"

class ProductPrices extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      errorFields: []
    };
  }

  componentWillMount() {

    if (this.props.params.id) {

      this.props.dispatch(getProduct(this.props.params.id));
      this.props.dispatch(getProductPrices(this.props.params.id));

    }

  }

  onChangePrice = (storeId, event) => {

    const value = event.currentTarget.value;
    let errorFields = this.state.errorFields;

    if (!isNaN(value)) {
      this.props.dispatch(setProductPrice(this.props.params.id, storeId, value));
      if (errorFields[storeId]) delete errorFields[storeId];
    } else {
      errorFields[storeId] = true;
    }

    this.setState({
      errorFields: errorFields
    });

  };

  render() {

    let {product, productPrices} = this.props;

    return (
      <div className="container">
        <div className="col-lg-12">

          <h3>Product's prices</h3>

          <form className="form-horizontal form-info">
            <div className="form-group">
              <label className="col-sm-2 control-label">Name</label>
              <div className="col-sm-10 product-info">
                {product.name}
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label">Type</label>
              <div className="col-sm-10 product-info">
                {product.type}
              </div>
            </div>
          </form>

          <br /> <br />
          <h4>Prices per stores</h4>

          <table className="table table-bordered">
            <thead>
            <tr>
              <th>Name</th>
              <th>City</th>
              <th className="col-lg-1">Price</th>
            </tr>
            </thead>
            <tbody>
            {productPrices.map((item, i) =>
              <tr>
                <td>{item.store_name}</td>
                <td>{item.store_city}</td>
                <td className={ this.state.errorFields[item.store_id] ? "has-error" : ""}>
                  <input type="text"
                         onBlur={this.onChangePrice.bind(this, item.store_id)}
                         className="form-control input-price"
                         defaultValue={item.price} />
                </td>
              </tr>
            )}
            </tbody>
          </table>

        </div>
      </div>
    );
  }
}

export default connect((state) => {
  return {
    product:  state.products.product,
    productPrices: state.products.productPrices
  };
})(ProductPrices)