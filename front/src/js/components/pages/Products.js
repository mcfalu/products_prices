import React from "react";

import { fetchProducts, removeProduct } from "../../actions/productsActions"
import {Link} from 'react-router'
import { getProductTypeTitle } from "../../helpers";

import { connect } from "react-redux"

class Products extends React.Component {

  componentWillMount() {

    this.props.dispatch(fetchProducts());
  }

  removeItem = (id) => {

    if (confirm("Are you sure want to remove selected product?")) {

      this.props.dispatch(removeProduct(id));

    }

  };

  render() {

    let { products } = this.props;

    return (
      <div className="container">
        <div className="col-lg-12">

          <h3>Products</h3>

          <Link className="btn btn-default" to="products-new">
            <i className="fa fa-plus" aria-hidden="true" /> New
          </Link>

          <br /><br />

          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Type</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {products.map((item, i) =>
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{getProductTypeTitle(item.type)}</td>
                  <td>
                    <div className="actions">

                      <Link title="Edit" to={{
                        pathname: '/products/' + item.id
                      }}>
                        <i className="fa fa-pencil" aria-hidden="true" />
                      </Link>

                      <Link title="Price" to={{
                        pathname: '/products/' + item.id + '/prices'
                      }}>
                        <i className="fa fa-dollar" aria-hidden="true" />
                      </Link>

                      <a href="#" title="Remove" onClick={this.removeItem.bind(this, item.id)}>
                        <i className="fa fa-trash" aria-hidden="true" />
                      </a>

                    </div>
                  </td>
                </tr>
              )}
            </tbody>
          </table>

        </div>
      </div>
    );
  }
}

export default connect((store) => {
  return {
    products: store.products.products
  };
})(Products);