import React from "react";

import { fetchStores } from "../../actions/productsActions"

import { connect } from "react-redux"

class Stores extends React.Component {

  componentWillMount() {

    this.props.dispatch(fetchStores());
  }

  render() {

    let { stores } = this.props;

    return (
      <div className="container">
        <div className="col-lg-12">

          <h3>Stores</h3>

          <table className="table table-bordered">
            <thead>
              <tr>
                <th>Id</th>
                <th>Name</th>
                <th>City</th>
              </tr>
            </thead>
            <tbody>
              {stores.map((item, i) =>
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.city}</td>
                </tr>
              )}
            </tbody>
          </table>

        </div>
      </div>
    );
  }
}

export default connect((store) => {
  return {
    stores: store.products.stores
  };
})(Stores);