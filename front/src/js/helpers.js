export const productTypes = {
  BOOK: 'Book',
  TOY: 'Toy',
  CLOTHES: 'Clothes'
};

export function getProductTypeTitle (key) {
  return productTypes[key] ? productTypes[key] : '';
}