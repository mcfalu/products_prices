export default function reducer(state = {
  products: [],
  product: {},
  stores: [],
  productPrices: []
}, action) {

  switch (action.type) {

    case "FETCH_PRODUCTS": {
      return {
        ...state,
        product: {},
        stores: [],
        products: action.payload,
        productPrices: []
      }
    }

    case "FETCH_STORES": {
      return {
        ...state,
        product: {},
        productPrices: [],
        stores: action.payload,
        products: []
      }
    }

    case "PRODUCT_ADDED": {

      let newProducts = [...state.products];
      newProducts.push(action.payload);

      return {
        ...state,
        products: newProducts,
      };

    }

    case "PRODUCT_FETCH_SUCCESS": {

      let newProduct = action.payload;

      return {
        ...state,
        product: newProduct,
      };

    }

    case "FETCH_PRODUCT_PRICES": {
      return {
        ...state,
        productPrices: action.payload,
      }
    }

  }

  return state
}