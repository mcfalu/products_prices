import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Products from "./components/pages/Products";
import Product from "./components/pages/Product";
import ProductPrices from "./components/pages/ProductPrices";
import Stores from "./components/pages/Stores";
import Layout from "./components/pages/Layout";

export default (
  <Route path="/" component={Layout}>
    <IndexRoute component={Products} />
    <Route path="products" name="products" component={Products} />
    <Route path="products-new" name="products-new" component={Product} />
    <Route path="products/:id" name="info" component={Product}/>
    <Route path="products/:id/prices" name="info" component={ProductPrices}/>
    <Route path="stores" name="stores" component={Stores} />
  </Route>
)